package Excercise5ThomasLao.excercise5thomaslao;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import Excercise5ThomasLao.excercise5thomaslao.databinding.ActivityMainBinding;

/**
 * Gradebook has UI (Fig. 1) and accesses DataProvider’s database.
 */

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private final String AUTHORITY = "edu.sjsu.android.dataprovider";
    private final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //inflate the layouts
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.add.setOnClickListener(this::addStudent);
        binding.get.setOnClickListener(this::getAllStudents);

    }

    /**
     * is the method that handles a click on "Add a Student" button. In this method, we use a
     * ContentValues object to store the data to the database. In put method, the key is the column name,
     * the value is the String you get from the user input. If you are using findViewById instead of binding,
     * but without having EditText objects, you need to explicitly cast them to EditText. For example:
     * ((EditText) findViewById(R.id.studentName)).getText().toString().
     */
    public void addStudent(View view) {
        ContentValues values = new ContentValues();
        values.put("name", binding.name.getText().toString());
        values.put("grade", binding.grade.getText().toString());
        /**
         * Toast message if successfully inserted
         */
        if (getContentResolver().insert(CONTENT_URI, values) != null) {
            Toast.makeText(this, "Student Added", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * getAllStudent is the method that handles a click on "Retrieve Students" button. Similarly, use
     * <p>
     * getContentResolver() to communicate with StudentsProvider identified by Uri.
     * <p>
     * The query method in StudentsProvider returns a Cursor object, which is used to get data from a database row by row
     */
    public void getAllStudents(View view) {
        /**
         * Sort by student name
         */
        try (Cursor c = getContentResolver().query(CONTENT_URI, null, null, null, "name")) {
            if (c.moveToFirst()) {
                String result = "Thomas Lao's Gradebook: \n";
                do {
                    for (int i = 0; i < c.getColumnCount(); i++) {
                        result = result.concat
                                (c.getString(i) + "\t");
                    }
                    result = result.concat("\n");
                } while (c.moveToNext());
                binding.result.setText(result);

            }
        }
    }

}

