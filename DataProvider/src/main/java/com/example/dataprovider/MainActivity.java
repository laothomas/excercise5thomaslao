package com.example.dataprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

/**
 * only provides the database and a
 * content provider without UI
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}