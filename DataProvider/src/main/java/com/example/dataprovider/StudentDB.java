package com.example.dataprovider;

/**
 * Class manages an SQL database
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class StudentDB extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "studentsDatabase";
    private static final int VERSION = 1;

    private static final String TABLE_NAME = "students";
    /**
     * PRIMARY KEY/COLUMN ATTRIBUTE
     */
    private static final String ID = "_id";
    /**
     * COLUMN ATTRIBUTE
     */
    private static final String NAME = "name";
    /**
     * COLUMN ATTRIBUTE
     */
    private static final String GRADE = "grade";

    static final String CREATE_TABLE =
            " CREATE TABLE " + TABLE_NAME +
                    " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + NAME + " TEXT NOT NULL, "
                    + GRADE + " TEXT NOT NULL);";

    public StudentDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    /**
     * You must implement the onCreate method, which is called when the database is first created.
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLE_NAME);
    }

    /**
     * You must implement the onUpgrade method, which is called when the database needs to be
     * upgraded (version changed).
     * <p>
     * Deletes the old table and create a new one.
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldV, int newV) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    /**
     * CUSTOM METHODS
     * implement these in StudentDB class instead of StudentProvider because we will use them we need to use the table name and the method "getWritableDatabase" to create and/or open a
     * database is provided in the parent class (SQLiteOpenHelper).
     */

    /**
     * Note that, when getWritableDatabase is first called, the database will be opened and cached, so we
     * can call this method every time we want to write to the database
     * <p>
     * According to the official documentation, "...you should not call this method from the application main thread...",
     * <p>
     * therefore, instead of setting a class variable for the database and initializing it in the constructor, we call
     * getWritableDatabase every time we need it.
     */


    public long insert(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        return database.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllStudents(String orderBy) {
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_NAME,
                new String[]{ID, NAME, GRADE},
                null, null, null, null, orderBy);
    }




}
